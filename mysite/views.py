__author__ = 'root'

from django.shortcuts import render
from books.models import Book
from django.http import HttpResponse, Http404
import datetime
from mysite.forms import ContactForm
from django.http import HttpResponseRedirect
from django.core.mail import send_mail



def hello(request):
    return HttpResponse("Hello world")


#def current_datetime(request):
#    now = datetime.datetime.now()
#    html = "<html><body>It is now %s.</body></html>" % now
#    return HttpResponse(html)


def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    #assert False
    return render(request, 'hours_ahead.html', {'hour_offset': offset, 'next_time': dt})


def current_datetime(request):
    now = datetime.datetime.now()
    #t = get_template('current_datetime.html')
    #html = t.render(Context({'current_date':now}))
    #return HttpResponse(html)
    return render(request, 'current_datetime.html', {'current_date': now})


#def book_list(request):
#    books = Books.ojbects.order_by('name')
#     return render(request, 'book_list.html', {'books': books})


def search(request):
    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            books = Book.objects.filter(title__icontains=q)
            return render(request, 'search_results.html', {'books': books, 'query': q})
    return render(request, 'search.html', {'error': error})


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            send_mail(
                cd['subject'],
                cd['message'],
                cd.get('email', 'hussien@toocoomedia.com'),
                ['hussien@toocoomedia.com'],
            )
            return HttpResponseRedirect('http://toocoomedia.com')
    else:
        form = ContactForm()
    return render(request, 'contact_form.html', {'form': form})



#test commit